package sn.isi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.isi.entities.ProductEntity;

public interface IProductRepository extends JpaRepository<ProductEntity, Integer> {
}
